extends Spatial

onready var particleObject = preload("res://objects/particle.tscn")
var timePassed = 1
var particleTypes = []
var particleCount = 0
var counterSpawn = 0
var spawnTrigger = 1
var radiation = 0
var score = 0
var submit_score = 0
var paused = true
var alarm_counter = 0
var get_score = false

func init():
	particleTypes.resize(36)
	for i in range(0,36):
		particleTypes[i] = 0
	radiation = 0
	timePassed = 1
	particleCount = 0
	counterSpawn = 0
	spawnTrigger = 1
	radiation = 0
	score = 0
	$hiscore.hide_me()

func _ready():
	$scientist.show_help()
	randomize()
	init()

func _process(delta):
	if !paused:
		timePassed += delta
		counterSpawn += delta
		radiation += delta * 0.001
		score += delta
		if counterSpawn >= spawnTrigger:
			counterSpawn = 0
			spawn_particle()
	if radiation < 0:
		radiation = 0
	if radiation > 8:
		alarm_counter += delta
		if radiation > 9.5:
			if alarm_counter >= 1:
				alarm_counter = 0
				$alarm.play()
		else:
			if alarm_counter >= 2:
				alarm_counter = 0
				$alarm.play()
	if radiation >= 10 && !$scientist.gameover:
		$scientist.game_over()

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if $scientist.gameover:
			$scientist.game_over()
			submit_score = score
			init()
			$hiscore.show_me()
		else:
			$scientist.show_help()
	if event.is_action_pressed("ui_help"):
		$scientist.show_help()
	if event.is_action_pressed('ui_toggleFullscreen'):
		OS.window_fullscreen = !OS.window_fullscreen
	if event is InputEventMouseButton:
		if Input.is_action_pressed('left_click'):
			OS.window_fullscreen = true

func spawn_particle():
	var speed = 1 + timePassed/60
	var maxTotal = 1 + timePassed/60
	if speed > 5:
		speed = 5
	if maxTotal > 6:
		maxTotal = 6
	if particleCount > maxTotal:
		return
	var p = particleObject.instance()
	p.moveDirection = Vector3.FORWARD.rotated(Vector3.UP, (randi()%8+0.5)/4*PI)*speed
	p.set_life(0.25 + (randi()%6)*0.25)
	add_child(p)
