extends Spatial

var player_name = ""
var player_list = null
var endpoint = "https://www.stupidrat.com/hotnucleus/hi/score.php"

func _ready():
	hide_me()
	$submit.connect("pressed", self, "_on_submit_released")
	$cancel.connect("pressed", self, "_on_cancel_released")
	$HTTPRequest.connect("request_completed", self, "_on_push_completed")
	$HTTPRequest2.connect("request_completed", self, "_on_get_completed")

func _process(delta):
	if len($get_name.text) > 9:
		$get_name.text = player_name.substr(0, 9)
		$get_name.cursor_set_column(9)
	player_name = $get_name.text

func hide_me():
	visible = false
	$MeshInstance.visible = false
	$submit.visible = false
	$cancel.visible = false
	$get_name.visible = false
	$Sprite3D2.visible = false

func show_me():
	visible = true
	$MeshInstance.visible = true
	$submit.visible = true
	$cancel.visible = true
	$get_name.visible = true
	$Sprite3D2.visible = true

func get_score():
	var command = "mode=get"
	var req = endpoint + "?" + command
	$HTTPRequest2.request(req)

func submit_score(player_name, player_score):
	var command = "mode=push&name=" + player_name + "&value=" + str(int(player_score))
	var req = endpoint + "?" + command
	$HTTPRequest.request(req)

func _on_submit_released():
	submit_score(player_name, get_parent().submit_score)

func _on_cancel_released():
	get_tree().change_scene("res://scenes/hiscores.tscn")

func _on_get_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	player_list = json.result

func _on_push_completed(result, response_code, headers, body):
	get_tree().change_scene("res://scenes/hiscores.tscn")
