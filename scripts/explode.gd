extends Spatial

var time = 3
var triggered_explosion = false

func _ready():
	$CPUParticles.emitting = true

func _process(delta):
	if !triggered_explosion:
		triggered_explosion = true
		$explosion.play()
	time -= delta
	if time <= 0:
		queue_free()
