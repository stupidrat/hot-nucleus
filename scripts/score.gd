extends Spatial

var score = 0
var t0 = 0
var t1 = 0
var t2 = 0
var t3 = 0
var v0 = 0
var v1 = 0
var v2 = 0
var v3 = 0

func _process(delta):
	score = get_parent().score
	t3 = score
	t2 = floor(score/10.0+0.1)
	t1 = floor(score/100.0+0.01)
	t0 = floor(score/1000.0+0.001)
	v3 += (t3-v3)*0.05
	v2 += (t2-v2)*0.05
	v1 += (t1-v1)*0.05
	v0 += (t0-v0)*0.05
	var offset = 180
	$roll0.rotation_degrees.x = -v0*36+offset
	$roll1.rotation_degrees.x = -v1*36+offset
	$roll2.rotation_degrees.x = -v2*36+offset
	$roll3.rotation_degrees.x = -v3*36+offset
