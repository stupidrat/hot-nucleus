extends Spatial

var request_delay = 0.1

func _ready():
	$LinkButton.connect("pressed", self, "_on_open")

func _input(event):
	if event.is_action_pressed("ui_accept"):
		get_tree().change_scene("res://scenes/splash.tscn")
		OS.window_fullscreen = true

func _process(delta):
	request_delay -= delta
	if request_delay <= 0:
		request_delay = 2
		$hiscore.get_score()
	if $hiscore.player_list:
		var scores = ""
		for i in range(0,100):
			var name = $hiscore.player_list["list"][i]["name"]
			var score = $hiscore.player_list["list"][i]["score"]
			if name != "":
				scores += str(i+1) + ". " + name + ":" + str(score) + "\n"
		$TextEdit.text = scores

func _on_open():
	OS.shell_open("https://twitter.com/c64cosmin")
