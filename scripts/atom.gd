extends Spatial

var energy = 1
var tremor = 0

func _process(delta):
	var game = get_node("/root/game")
	if game == null:
		energy = 1
	else:
		energy = game.radiation + 0.1
	tremor = (energy*0.2) + 0.03
	$core.rotation_degrees.x += energy*1.2
	$core.rotation_degrees.y += energy*2.3
	$core.rotation_degrees.z += energy*3.4
	$core.translation = Vector3(randf()-0.5,0,randf()-0.5)*tremor*3
