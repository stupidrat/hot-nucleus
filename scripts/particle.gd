extends Spatial

var moveDirection = Vector3.ZERO
var type = 0
var life = 0
var life_max = 0
var key_pressed = false
var tremor = 0
var tremor_target = 0
var weapon_target0 = null
var weapon_target1 = null
var center_atom = null
var dying = -1
var triggered_neutralize = false
var bonus = 1
onready var explosion = preload("res://objects/explode.tscn")

func _ready():
	var try_spawn = 0
	type = randi()%36
	while try_spawn < 5 && get_parent().particleTypes[type] == 1:
		type = randi()%36
		try_spawn += 1
	if get_parent().particleTypes[type] == 1:
		queue_free()
	else:
		init()

func init():
	$center/Sprite3D.frame = type
	get_parent().particleTypes[type] = 1
	get_parent().particleCount += 1
	$ray.set_mesh(PlaneMesh.new())
	$ray.get_mesh().size.x = 2
	$ray.get_mesh().surface_set_material(0, preload("res://textures/lightning.tres"))
	$load.set_mesh(PlaneMesh.new())
	$load.get_mesh().size.x = 2
	$load.get_mesh().surface_set_material(0, preload("res://textures/lightning.tres"))
	weapon_target0 = get_node("../weapons/weapon0").global_transform.origin
	weapon_target1 = get_node("../weapons/weapon1").global_transform.origin
	center_atom = get_node("../atom").global_transform.origin

func _process(delta):
	if get_parent().get_node("scientist").gameover:
		queue_free()
	update_damage_ray()
	update_load_ray()
	if !get_parent().paused:
		translation = translation + moveDirection * delta
		if key_pressed:
			life -= delta
			tremor_target = 0.1 + (1.0-life/life_max)*0.4
			if life <= 0:
				life = 0
				if dying != -1:
					dying = 3
				$center/CPUParticles.emitting = true
				bonus -= delta
				if bonus <= 0:
					bonus = 0
				get_parent().radiation += 0.003
		else:
			tremor_target = (1.0-life/life_max)*0.2
			if life <= 0:
				$center/CPUParticles.emitting = false
				if dying == -1:
					dying = 3
					if !triggered_neutralize:
						triggered_neutralize = true
						$neutralize.play()
					$die.emitting = true
		if dying > 0:
			dying -= delta
			if dying <= 0:
				delete_me()
				get_parent().score += bonus*10
				get_parent().radiation -= 0.1
	if translation.length() > 20 && dying == -1:
		delete_me()
		var expl = explosion.instance()
		get_parent().add_child(expl)
		expl.global_transform.origin = global_transform.origin
		get_parent().radiation += 0.05
	tremor += (tremor_target - tremor)*0.1
	$center.translation = Vector3(randf()-0.5,0,randf()-0.5)*tremor*2

func _input(ev):
	if get_parent().paused:
		return
	var expectedKey = KEY_0 + type
	if type >= 10:
		expectedKey = KEY_A + type - 10
	if Input.is_key_pressed(expectedKey):
		key_pressed = true
	else:
		key_pressed = false

func delete_me():
	get_parent().particleTypes[type] = 0
	get_parent().particleCount -= 1
	queue_free()

func set_life(life_amount):
	life = life_amount
	life_max = life_amount

func update_load_ray():
	var target = center_atom
	var distance_vector = global_transform.origin - target
	$load.visible = key_pressed && life <= 0
	$load.global_transform.origin = global_transform.origin - distance_vector / 2
	$load.global_transform = $load.global_transform.looking_at(target, Vector3.UP)
	$load.get_mesh().size.y = distance_vector.length()

func update_damage_ray():
	var target = weapon_target0
	if translation.x > 0:
		target = weapon_target1
	var distance_vector = global_transform.origin - target
	if key_pressed && !$laser.playing:
		$laser.play()
	if !key_pressed && $laser.playing:
		$laser.stop()
	$ray.visible = key_pressed
	$ray.global_transform.origin = global_transform.origin - distance_vector / 2
	$ray.global_transform = $ray.global_transform.looking_at(target, Vector3.UP)
	$ray.get_mesh().size.y = distance_vector.length()
	$rays.emitting = key_pressed
	$rays.global_transform.origin = target
