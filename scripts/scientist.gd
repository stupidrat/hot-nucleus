extends Spatial

var help = false
var msgNr = 0
var translateTarget = 0
var gameover = false

func _ready():
	pass # Replace with function body.

func _process(delta):
	translation.z += (translateTarget-translation.z)*delta

func show_help():
	if gameover:
		return
	if !help:
		help = true
		get_parent().paused = true
		translateTarget = 0
		$bubble.visible = true
		msgNr = 0
	else:
		msgNr += 1
		if msgNr == 5:
			help = false
			$bubble.visible = false
			get_parent().paused = false
			translateTarget = 4
			return
	show_msg()

func game_over():
	if !gameover:
		gameover = true
		get_parent().paused = true
		translateTarget = 0
		$bubble.visible = true
		msgNr = 5
	else:
		gameover = false
		translateTarget = 4
		$bubble.visible = false
		return
	show_msg()

func show_msg():
	$bubble/msg0.visible = false
	$bubble/msg1.visible = false
	$bubble/msg2.visible = false
	$bubble/msg3.visible = false
	$bubble/msg4.visible = false
	$bubble/msg5.visible = false
	get_node("bubble/msg" + str(msgNr)).visible = true
