extends Spatial

func _ready():
	var atomObj = load("res://objects/atom.tscn")
	for i in range(0,50):
		var atom = atomObj.instance()
		atom.translation = Vector3(((randi()%11)/11.0)*400-200, randf()*200, ((randi()%11)/11.0)*300-150)
		add_child(atom)
