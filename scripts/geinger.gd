extends Spatial

var radiation = 0
var time = 0

onready var geingerSounds = [
	preload("res://sounds/geinger0.wav"),
	preload("res://sounds/geinger1.wav"),
	preload("res://sounds/geinger2.wav"),
	preload("res://sounds/geinger3.wav")
]

func _process(delta):
	play_geinger()
	time += delta
	radiation = get_parent().radiation
	var tremor = randf() + pow(cos(time),31) + pow(cos(time*2),31) + pow(cos(time*3),31) + pow(cos(time*5),31)
	tremor *= (radiation/10.0)+1+radiation
	var angle = 88 - (tremor + radiation*18)
	if angle > 88:
		angle = 88
	if angle < -88:
		angle = -88
	$geinger/needle.rotation_degrees.y = angle

func play_geinger():
	var chance = max(1,int(20-radiation*3))
	if randi()%chance == 0:
		$sound.stream = geingerSounds[randi()%4]
		$sound.play()
