extends Spatial

func _input(event):
	if event.is_action_pressed("ui_accept"):
		get_tree().change_scene("res://scenes/game.tscn")
		OS.window_fullscreen = true
	if Input.is_key_pressed(KEY_F2):
		get_tree().change_scene("res://scenes/hiscores.tscn")
		OS.window_fullscreen = true
	if event.is_action_pressed('ui_toggleFullscreen'):
		OS.window_fullscreen = !OS.window_fullscreen
	if event is InputEventMouseButton:
		if Input.is_action_pressed('left_click'):
			OS.window_fullscreen = true
