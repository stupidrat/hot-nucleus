extends Spatial

var energy = 1
var electro_rot = Vector3()
var electro_rate = Vector3()

func _ready():
	electro_rot.x = randf()
	electro_rot.y = randf()
	electro_rot.z = randf()
	electro_rot.x = randf()
	electro_rate.y = randf()*0.01+0.01
	electro_rate.z = randf()*0.01+0.01

func _process(delta):
	energy = get_parent().energy * 0.5 + 0.5
	electro_rot.x += energy*0.1
	electro_rot.y += energy*electro_rate.y
	electro_rot.z += energy*electro_rate.z
	$electron.transform = Transform.IDENTITY.rotated(Vector3.UP, electro_rot.x).rotated(Vector3.RIGHT, electro_rot.y).rotated(Vector3.UP, electro_rot.z)
